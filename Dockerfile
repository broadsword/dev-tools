FROM ubuntu:xenial

ENV WATCHMAN_VERSION v4.7.0
ENV NODE_VERSION 6.x

WORKDIR /app

RUN apt-get update
RUN apt-get install -y curl apt-utils
RUN curl -sL https://deb.nodesource.com/setup_${NODE_VERSION} | bash -
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update
RUN apt-get install -y git \
autoconf \
automake \
build-essential \
python-dev \
libtool \
pkg-config \
openssl \
yarn
RUN yarn global add create-react-native-app
RUN git clone https://github.com/facebook/watchman.git
RUN cd watchman && git checkout ${WATCHMAN_VERSION} && ./autogen.sh && ./configure && make && make install

EXPOSE 19000 19001

ENTRYPOINT bash
