## Getting Started

This repo contains scripts and files for react native development with docker containers

The repo contains:

* Dockerfile - for building the docker image
* create-react-server.sh - for creating the docker container from the image
* run-react-server.sh - for starting the docker container and react native dev server
* .eslintrc.json - a default config file for eslint to help enforce coding standards

## Setting Up The Workflow

By default docker requires you to run it with `sudo` but this is annoying so we will fix that:

1. Open up a terminal
1. Run `sudo groupadd docker` (this will probably say it already exists)
1. `sudo gpasswd -a $USER docker` (`$USER` will automatically put in your username)
1. Log out then back in


Using the bash scripts won't be very practical if you always have to navigate to their folders to use them. We can make it so we can run them from anywhere:

1. Open up a terminal
1. Enter `echo $PATH | grep $HOME/bin`, if it prints a line with text skip to the next section, otherwise
1. Follow the steps [here](https://askubuntu.com/a/60221) to add `~/bin` to your path
1. Log out then back in

What we need to do next is called linking. This means that we can create a file that is linked to another file in a different location.

You can read about linking [here](https://en.wikipedia.org/wiki/Symbolic_link#Overview).

We will create files in `~/bin` that link to the .sh scripts for us

Continuing on from the last step:

1. Create your bin folder with `mkdir ~/bin` if it doesn't already exist
1. Ok, now go back to the folder that contains the .sh files
1. Run `ln -sf $PWD/create-react-server.sh $HOME/bin/create-react-server`
1. Then `ln -sf $PWD/run-react-server.sh $HOME/bin/run-react-server`

You should now be able to execute these scripts from anywhere when in your terminal

###### Having some trouble?

* Make sure docker is installed
* Make sure the docker service is running with `sudo service docker start`
* Make sure the .sh scripts are executable with `chmod +x script_name.sh`
* Check that the file links still link and are valid. Maybe after running `git pull` the links broke
* If the server is appearing to run like normal but you can't connect, make sure there isn't a firewall in the way

### Dockerfile

This just describes to docker what we want our image to have in it

To use this:

1. Open up a terminal and navigate to the folder which contains this
1. Run `docker build -t react-server .` (the `.` at the end just means look in this folder)

That is it! Nice and simple

`The steps here assume that you are following the workflow guide`

### create-react-server.sh

This script will take a project name you give it and create a container that holds all the JS and other tools needed to develop react native

It starts by creating a folder called 'app' to hold the react native files and then mounts that folder to the container

After it has created the container it will create a react native project and add the eslint plugins

Now whatever is in that folder will also be in the container. This is where all the development files go

When the files in the `app` folder are created, they are owned by `root`, so a `sudo chmod` command is used. This will have to be changed in the future

If anything fails it will attempt to clean up by deleting files and folders created

To use this:
    
1. Run `create-react-server AppName`

All done

### run-react-server.sh

This script will start the container if required and then run `yarn run start` in the container to start the server

When the server is closed, the script will shut down the container

Sometimes when connecting to the server from your phone for the first time it will say the build failed

Just exit the app expo launched and connect again. It should work

To use this:

1. Run `run-react-server AppName`
1. Press `Ctrl-c` to exit the server and the container will stop automatically

Should work 
