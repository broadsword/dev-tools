#!/bin/bash

RED='\033[1;31m'
NC='\033[0m'
BLUE='\033[1;34m'
GREEN='\033[1;32m'

CONTAINER='registry.gitlab.com/broadsword/dev-tools/react-server:ubuntu-yarn'

function clean_up_on_fail {
    echo -e "${BLUE}Cleaning up a bit...${NC}"
    docker stop "$1"-server
    docker rm "$1"-server
    # if the app directory was created we want to remove it
    if [ -d "app" ]
    then
        echo -e "${BLUE}Remove the app folder that was created:${NC}"
        sudo rm -r app
    fi
}

function creating_container_volume_prompt {
    echo ""
    echo -e "${BLUE}########################################"
    echo "# Creating container and shared volume #"
    echo -e "########################################${NC}"
    echo ""
}

function creating_app_prompt {
    echo ""
    echo -e "${BLUE}################################"
    echo "# Creating RN app on container #"
    echo -e "################################${NC}"
    echo ""
}

function change_ownership_prompt {
    echo ""
    echo -e "${BLUE}############################################"
    echo "# We need to change ownsership permissions #"
    echo -e "############################################${NC}"
    echo ""
}

# check at least a name was given
if [ -z $1 ]
then
    echo -e "${RED}No project name argument supplied${NC}"
    exit 1
fi

# create the docker container and shared volume
creating_container_volume_prompt
docker run -dit --net=host --name "$1"-server -v "$PWD"/app:/app "$CONTAINER"
if [ $? != 0 ]
then
    echo -e "${RED}Failed on docker run command${NC}"
    echo -e "${BLUE}Does the container already exist?${NC}"
    exit 2
fi

# use create-react-native-app in container to create base of project
creating_app_prompt
docker exec "$1"-server create-react-native-app .
if [ $? != 0 ]
then
    echo -e "${RED} Failed on docker exec create-react-native-app${NC}"
    clean_up_on_fail $1
    exit 3
fi
# install eslint plugins for linting
docker exec "$1"-server yarn add eslint eslint-plugin-react eslint-plugin-react-native
if [ $? != 0 ]
then
    echo -e "${RED} Failed on docker exec npm install eslint plugins${NC}"
    clean_up_on_fail $1
    exit 4
fi

# the files are created by root user in the container so they are currently
# owned by the root user.
# change the files and folders to be owned by you.
change_ownership_prompt
sudo chown -R "$USER":users "$PWD"/app/.
if [ $? != 0 ]
then
    echo -e "${RED}Failed to change permissions${NC}"
    clean_up_on_fail $1
    exit 5
fi

docker stop "$1"-server

echo -e "${GREEN}done${NC}"
