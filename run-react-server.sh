#!/bin/bash

RED='\033[1;31m'
NC='\033[0m'
BLUE='\033[1;34m'
GREEN='\033[1;32m'

# check at least a name was given
if [ -z "$1" ]
then
    echo -e "${RED}No project name argument supplied${NC}"
    exit 1
fi

# start the react server in the container
docker exec -it "$1"-server yarn run start
STARTED=$?

# check if the react server start command failed
# maybe the container needs to be started
if [ $STARTED != 0 ]
then
    echo -e "${BLUE}Attempting to start the container...${NC}"
    docker start "$1"-server
    STARTED=$?
    docker exec -it "$1"-server yarn run start
fi

# before exiting, make sure the container is stopped
# only runs if the container was started
if [ $STARTED == 0 ]
then
    echo -e "${BLUE}Stopping the container${NC}"
    docker stop "$1"-server
fi
